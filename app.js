'use strict';

// To be loaded in index.html

import $ from 'jquery';
import _ from 'lodash';
import PIXI from 'pixi.js';
import Sprite from './lib/sprite';

console.log($.fn.jquery);
console.log(_.VERSION);
console.log(PIXI.VERSION);

console.log(new Sprite());
