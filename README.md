This repository is simply some testing with jspm.io and babeljs.io to experiment with ESNext.

## browser

```bash
npm install -g live-server jspm
# lovely output

jspm install
# more lovely output

live-server # check the javascript console for output
```

See `index.html` and `app.js` for more details.

## node_example

This example doesn't actually use `jspm`, but I wanted an es6 node.js module example here as well.

See the `node_example/` directory for a simple example using es6 modules with node.js
