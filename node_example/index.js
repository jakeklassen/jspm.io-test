'use strict';

// You'll need two modules to make this work, run:
// npm i babel-core es6-module-loader -S

var System = require('es6-module-loader').System;
System.transpiler = 'babel';

System.import('node_example/app').then(function(app) {
  app.run(__dirname);
}).catch(function(err) {
  console.log('err', err);
});
