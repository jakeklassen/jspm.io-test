'use strict';

// Can we use require() syntax here as well?!
var os = require('os');
console.log(`You're hostname is ${os.hostname()}`);

import Shape from './lib/shape';

export function run(appdir) {
  console.log(`You are running the app from "${appdir}"`);

  console.log(new Shape());
}
