'use strict';

export default class Shape {
  constructor(sides = 1) {
    this.sides = sides;
  }
}
